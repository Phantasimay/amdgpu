# AMD Firmware Linux + Extra

Possibly missing GPU [firmware linux](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/amdgpu).

## Usage:

1. If you get `W: Possible missing firmware /lib/firmware/amdgpu/...... for module amdgpu`.
1. Run `git clone git@gitlab.com:Phantasimay/amdgpu.git && cd amdgpu`.
1. Copy all file on ~/amdgpu to `/lib/firmware/amdgpu`.
1. Reboot.
